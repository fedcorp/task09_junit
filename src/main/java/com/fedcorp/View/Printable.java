package com.fedcorp.View;

import java.io.IOException;

@FunctionalInterface
public interface Printable {
    void print() throws IOException;
}
